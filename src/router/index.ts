import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Calculator from '../views/Calculator.vue'
import History from '../views/History.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'History',
    component: History
  },
  {
    path: '/calc',
    name: 'Calculator',
    component: Calculator
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
