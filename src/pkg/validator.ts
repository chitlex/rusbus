export function numberRules(name: string, isInteger: boolean, min: number, max: number, isRequired: boolean) {
    const rules = []
    if (isRequired) {
        rules.push((v: any) => !!v || `Field "${name}" is required`)
    }
    rules.push((v: any) => !isNaN(v) || 'Value is not number')
    if (isInteger) {
        rules.push((v: any) => !isNaN(v) && Number.isInteger(parseInt(v)) || 'Value in not integer')
    }
    rules.push((v: any) => v >= min || `The value cannot be less than ${min}`)
    rules.push((v: any) => v <= max || `The value cannot be greater than ${max}`)
    return rules
}