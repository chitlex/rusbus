const pf = parseFloat

export function calculateEstimatedJobCosts(rate: string, duration: string, material: string, materialTax: string) {
    const r = pf(rate)
    const d = pf(duration)
    const m = pf(material)
    const mt = pf(materialTax)
    return m === 0 ? (r * d) : (r * d)+(m + 100*mt/m)
}

export function calculateMarkUp(profit: string, companyOverhead: string) {
    return pf(profit) + pf(companyOverhead)
}

export function calculateJobCost(markUp: number) {
    return 100-markUp
}

export function calculateSellingPrice(estimatedJobCosts: number, jobCost: number) {
    return estimatedJobCosts/jobCost*100
}

export function calcSellingPrice(rate: string, duration: string, material: string, materialTax: string, profit: string, companyOverhead: string) {
    const estimatedJobCosts = calculateEstimatedJobCosts(rate, duration, material, materialTax)
    const markUp = calculateMarkUp(profit, companyOverhead)
    const jobCost = calculateJobCost(markUp)
    return calculateSellingPrice(estimatedJobCosts, jobCost)
}