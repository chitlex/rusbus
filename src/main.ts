import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import store from './store'
import router from './router'
import {Load} from "@/store/types";

Vue.config.productionTip = false

store.dispatch(Load)

new Vue({
  vuetify,
  store,
  router,
  render: h => h(App)
}).$mount('#app')
