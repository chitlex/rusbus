import Vue from 'vue'
import Vuex from 'vuex'
import localforage from 'localforage'
import {
  ClearHistory,
  ExportToCSV,
  ImportFromCSV,
  Load,
  ResetForm,
  Save,
  DeleteItem,
  FillCalculator, SaveFormData, LoadFormData
} from "@/store/types";
import {calcSellingPrice} from "@/pkg/calc";
import {saveToFile} from "@/pkg/file";

Vue.use(Vuex)

const initCalcFormData = {
  rate: 0,
  duration: 0,
  material: 0,
  materialTax: 0,
  profit: 30,
  companyOverhead: 22.67,
}

export default new Vuex.Store({
  state: {
    calcForm: {...initCalcFormData},
    history: [],
    headers: [
      {
        text: 'Timestamp',
        value: 'timestamp',
        sortable: true,
      },
      {
        text: 'Est. Labor Tech Salary for 1 order',
        value: 'rate',
        sortable: true,
      },
      {
        text: 'Est. Hours per job required',
        value: 'duration',
        sortable: true,
      },
      {
        text: 'Est. Material',
        value: 'material',
        sortable: true,
      },
      {
        text: 'Material Tax',
        value: 'materialTax',
        sortable: true,
      },
      {
        text: 'Profit',
        value: 'profit',
        sortable: true,
      },
      {
        text: 'Company Overhead',
        value: 'companyOverhead',
        sortable: true,
      },
      {
        text: 'Selling Price',
        value: 'sellingPrice',
        sortable: true,
      }
    ]
  },
  mutations: {
  },
  actions: {
    [Load]: (ctx: any) => {
      const data = localStorage.getItem('history')
      if (data) {
        const rows = JSON.parse(data)
        if (rows && rows.length) {
          Vue.set(ctx.state, "history", rows)
        }
      }
    },
    [ResetForm]: (ctx: any) => {
      Vue.set(ctx.state, "calcForm", {...initCalcFormData})
    },
    [Save]: (ctx: any) => {
      const rows = [...(ctx.state.history || [])]
      const cf = ctx.state.calcForm
      const sellingPrice = calcSellingPrice(cf.rate, cf.duration, cf.material, cf.materialTax, cf.profit, cf.companyOverhead).toFixed(2)
      rows.push({
        ...cf,
        sellingPrice,
        timestamp: new Date().toISOString(),
      })
      Vue.set(ctx.state, "history", rows)
      localStorage.setItem("history", JSON.stringify(rows))
    },
    [ClearHistory]: (ctx: any) => {
      Vue.set(ctx.state, "history", [])
      localStorage.setItem("history", JSON.stringify([]))
    },
    [ExportToCSV]: (ctx: any) => {
      const csvRows = []
      csvRows.push(`"${ctx.state.headers.map((h: any) => h.text).join('";"')}"`)

      ctx.state.history.forEach((item: any) => {
        const list: any[] = []
        ctx.state.headers.forEach((header: any) => {
          list.push(item[header.value])
        })
        csvRows.push(`"${list.join('";"')}"`)
      })

      const data = csvRows.join('\n')
      saveToFile(data, "history.csv")
    },

    [ImportFromCSV]: (ctx: any, payload: any) => {
      const rows = payload.data.split('\n')
      const dataRows = rows.slice(1)
      const history: any = []

      dataRows.forEach((row: any) => {
        const fields = row.split(';')
        const item: any = {}
        ctx.state.headers.forEach((h: any, index: number) => {
          item[h.value] = fields[index].replaceAll('"', '')
        })
        history.push(item)
      })

      Vue.set(ctx.state, "history", history)
      localStorage.setItem("history", JSON.stringify(history))

    },

    [DeleteItem]: (ctx: any, payload: any) => {
      const history = ctx.state.history.filter((item: any) => item.timestamp !== payload.timestamp)
      Vue.set(ctx.state, "history", history)
      localStorage.setItem("history", JSON.stringify(history))
    },

    [FillCalculator]: (ctx: any, payload: any) => {
      Vue.set(ctx.state, "calcForm", payload)
    },
    [SaveFormData]: (ctx: any) => {
      const formDataString = JSON.stringify(ctx.state.calcForm)
      localforage.setItem('formData', formDataString)
          .then(() => {
            console.log('form data saved')
          })
          .catch(() => {
            console.log('failed to save form data')
          })
    },
    [LoadFormData]: async (ctx: any) => {
      try {
        const data = await localforage.getItem('formData')
        if (typeof data === 'string') {
          Vue.set(ctx.state, "calcForm",  JSON.parse(data))
        }
      } catch(err) {
        console.error('failed to load form data', err)
        Vue.set(ctx.state, "calcForm",  {...initCalcFormData})
      }
    }
  },
  modules: {
  }
})
